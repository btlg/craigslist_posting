require 'rubygems'
require 'nokogiri'

class PostHtml
   # constructor method
   def initialize(html_file)
      @page = Nokogiri::HTML(open(html_file))
      @path = html_file.gsub(/\/[^\/]*\.html$/,"")
      puts "Page source loaded."
   end

   def get_title
      title = @page.css("h2[class='postingtitle']").text.gsub(/\n/, "")
      puts "Got title: #{title}"
      title
   end

   def get_body
      body = @page.css("section#postingbody").inner_html.gsub("\n","").gsub("\t","")
      puts "Got body: #{body}"
      body
   end

   def get_image
      begin
         image = @page.css("img#iwi")[0]["src"].to_s
         #remove first character of the image(relative path) and append it to the directory path
         puts "Got image: #{image}"
         image_path = @path+image[1..-1]
      rescue 
         puts "No image."
         return nil
      end
   end 

   def get_type
      type = @page.xpath("//header/div/div[1]/span[4]/a").text
      if type.include?("housing")
         if @page.xpath("//header/div/div[1]/span[5]/a").text.include?("wanted")
            type = "hw"
         else 
            type = "ho"
         end
      elsif type.include?("all services offered")
         type = "so"
      end
      puts "Got type: #{type}"
      type
   end

   def get_category
      category = @page.xpath("//header/div/div[1]/span[5]/a").text
      if category.include?("real estate services")
         category = "105"
      elsif category.include?("financial services")
         category = "104"
      elsif category.include?("real estate - by owner")
         category = "143"
      elsif category.include?("real estate wanted")
         category = "121"
      end
      puts "Got category: #{category}"
      category
   end

   def get_location
      location = @page.xpath("//header/div/div[1]/span[3]/a").text
      if location.include?("district of columbia")
         location = "1"
      elsif location.include?("maryland")
         location = "3"
      end 
      puts "Got location: #{location}"
      location
   end


end
