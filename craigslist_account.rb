require 'celerity'

class CraigslistAccount
   
   LOGIN_LINK = "https://accounts.craigslist.org/login"
   LOGOUT_LINK = 'https://accounts.craigslist.org/logout'

   # constructor method
   def initialize(email,password, browser)
      @email, @password = email, password
      @browser = browser
      @browser.webclient.java_script_enabled = false
      puts "Account #{email} loaded"
   end

   # instance method by default it is public
   def login
      @browser.goto(LOGOUT_LINK)
      @browser.goto(LOGIN_LINK)
      input_box = @browser.text_field(:id, "inputEmailHandle")
      input_box.set(@email)
      input_box_pass = @browser.text_field(:id, "inputPassword")
      input_box_pass.set(@password)
      go_button = @browser.button(:type, "submit")
      go_button.click
      unless @browser.html.include?("Showing all postings")
         raise "Login Failed!"
      end
      puts "Logged in"
   end

   def start_posting(city)
      @browser.select_list(:name, "areaabb").select(city)
      go_button = @browser.button(:value, "go")
      go_button.click
      unless @browser.html.include?("choose type")
         raise "Start Posting Failed!"
      end
      puts "Started posting"
   end
   
   def choose_type(type)
      radio = @browser.radio(:value, type)
      radio.set
      continue = @browser.button(:value, "continue") 
      continue.click
      unless @browser.html.include?("choose a category")
         raise "Choose Type Failed!"
      end
      puts "Chose #{type} as type of posting"
   end

   def choose_category(category)
      radio = @browser.radio(:value, category)
      radio.set
      continue = @browser.button(:value, "continue") 
      continue.click
      unless @browser.html.include?("choose nearest area")
         raise "Choose Category Failed!"
      end
      puts "Chose #{category} as category"
   end

   def choose_area(area)
      radio = @browser.radio(:value, area)
      radio.set
      continue = @browser.button(:value, "continue") 
      continue.click
      unless @browser.html.include?("create posting")
         raise "Choose Area Failed!"
      end
      puts "Chose #{area} as location"
   end

   def edit_post(title, body, postal_code)
      title_box = @browser.text_field(:id, "PostingTitle")
      title_box.set(title)
      body_box = @browser.text_field(:id, "PostingBody")
      body_box.set(body)
      postal_box = @browser.text_field(:id, "postal_code")
      postal_box.set(postal_code)
      continue = @browser.button(:value, "continue") 
      continue.click
      unless !@browser.html.include?("add map")
         raise "Edit Post Failed!"
      end
      puts "Edit post completed"
   end   

   def add_map
      continue = @browser.button(:value, "continue") 
      continue.click
      unless @browser.html.include?("create posting")
         raise "Add Map Failed!"
      end
      puts "Add Map completed"
   end

   def edit_image(image)
      puts "Enter edit_image"
      unless image.nil?
         @browser.file_field(:type => "file").set(image)
         open = @browser.button(:name, "go")
         open.click
      end
      done = @browser.button(:class, "done bigbutton") 
      if !done.exist?
         done = @browser.button(:class, "bigbutton")
      end 
      done.click
      unless @browser.html.include?("publish")
         raise "Edit Image Failed!"
      end
      puts "Edit image completed"
   end 

   def publish
      publish = @browser.button(:class, "done bigbutton")
      if !publish.exist?
         publish = @browser.button(:class, "bigbutton")
      end 
      publish.click
      puts "Post published"
   end

   def logout 
      @browser.goto(LOGOUT_LINK)
   end
 
end
