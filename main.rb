require 'celerity'
require 'craigslist_account'
require 'post_html'
require 'email_account'

begin 
	
	files = Dir["ads/*.html"]
	accounts = File.readlines("pvas.txt")
	POSTAL_CODE = "20722"
	CITY = "washington, dc, dc, us"
	browser = Celerity::Browser.new

	files.each_with_index do |file, index|

		email = accounts[2*index].strip
		password = accounts[2*index+1].strip
		chf = PostHtml.new(file)
		title = chf.get_title
		body = chf.get_body
		image = chf.get_image
		type = chf.get_type
		category = chf.get_category
		location = chf.get_location 


		ca = CraigslistAccount.new(email, password, browser)
		ca.login
		ca.start_posting(CITY)
		ca.choose_type(type)
		ca.choose_category(category)
		ca.choose_area(location)
		ca.edit_post(title,body,POSTAL_CODE)
		#ca.add_map
		ca.edit_image(image)
		ca.publish
		ca.logout
		if browser.html.include?("FURTHER ACTION IS REQUIRED")
			email_account = EmailAccount.new(email, password)
			email_account.login
			link = email_account.read_email
			unless link.nil?
				browser.goto(link)
			end
			email_account.logout
		end
	end
	browser.clear_cache
	browser.close
end